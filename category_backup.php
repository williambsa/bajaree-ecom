<?php
include 'config/config.php';

$page_title = get_option('SITE_DEFAULT_META_TITLE');
$page_description = get_option('SITE_DEFAULT_META_DESCRIPTION');
$page_keywords = get_option('SITE_DEFAULT_META_KEYWORDS');
$site_author = $config['CONFIG_SETTINGS']['SITE_AUTHOR'];
$categoryId = 0;
$categoryArray = array();
$productSubCategoryArray = array();
$subCategoryArray = array();


if (isset($_REQUEST['category_id']) AND $_REQUEST['category_id'] > 0) {
    $categoryId = intval($_REQUEST['category_id']);
} else {
    $link = 'index.php?err=' . base64_encode('Url is not correct !!');
    redirect($link);
}


/* category query */
$categoySql = "SELECT category_id,category_name,category_parent_id, category_logo FROM categories WHERE category_id=$categoryId LIMIT 1";
$categoySqlResult = mysqli_query($con, $categoySql);

if ($categoySqlResult) {
    $categoryArray = mysqli_fetch_object($categoySqlResult);
    mysqli_free_result($categoySqlResult);
} else {
    if (DEBUG) {
        die('categoySqlResult error : ' . mysqli_error($con));
    } else {
        die('categoySqlResult query fail');
    }
}

if (count($categoryArray) < 1) {
    $link = 'index.php?err=' . base64_encode('Category not found !!');
    redirect($link);
}
/* // category query */
printDie($categoryArray);
$currentCategoryName = $categoryArray->category_name;
$page_title .=' ' . $currentCategoryName;
/* sub categories product query */

echo $subCategoySql = "SELECT 
    
products.product_id, products.product_title, products.product_price, products.product_show_as_new_from, products.product_show_as_new_to, products.product_show_as_featured_from, products.product_show_as_featured_to,
product_images.PI_file_name,
categories.category_id, categories.category_name, categories.category_parent_id, categories.category_priority   

FROM products

LEFT JOIN product_images ON product_images.PI_product_id = products.product_id
LEFT JOIN product_categories ON product_categories.PC_product_id = products.product_id
LEFT JOIN categories ON categories.category_id = product_categories.PC_category_id
WHERE categories.category_parent_id = $categoryId AND products.product_status ='active'
";

$subCategoySqlResult = mysqli_query($con, $subCategoySql);

if ($subCategoySqlResult) {
    while ($subCategoySqlResultRowObj = mysqli_fetch_object($subCategoySqlResult)){
        $current_cat_id=$subCategoySqlResultRowObj->category_id;
        $current_cat_name=$subCategoySqlResultRowObj->category_name;
        $productSubCategoryArray[$current_cat_id][] =$subCategoySqlResultRowObj;
        $subCategoryArray[$current_cat_id]=array('id'=>$current_cat_id,'name'=>$current_cat_name);
    } 
    
    
            
            
    mysqli_free_result($subCategoySqlResult);
} else {
    if (DEBUG) {
        die('subCategoySqlResult error : ' . mysqli_error($con));
    } else {
        die('subCategoySqlResult query fail');
    }
}

printDie($subCategoryArray);
/* //sub categories product query */
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title><?php echo $page_title; ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="<?php echo $page_description; ?>">
        <meta name="keywords" content="<?php echo $page_keywords; ?>">
        <meta name="author" content="<?php echo $site_author; ?>">

        <?php include basePath('header_script.php'); ?>
        <script src="<?php echo baseUrl(); ?>ajax/index/main.js"></script>
    </head>

    <body>


        <div id="wrapper">
            <div id="header">
                <div class="navbar navbar-default navbar-fixed-top megamenu">
                    <div class="container-full">
                        <?php include basePath('headertop.php'); ?>
                        <!--/.headertop -->
                        <?php include basePath('header_mid.php'); ?>
                        <!--/.headerBar -->

                        <?php include basePath('header_menu.php'); ?>
                        <!--/.menubar --> 
                    </div>
                </div>

            </div>
            <!-- header end -->

            <div style="clear:both"></div>
            <div class="w100 mainContainer innerPadd">


                <div class="container">
                    <?php include basePath('alert.php'); ?>
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-2">
                            <h3 class="categoryName"><?php echo $currentCategoryName; ?></h3>
                            <ul class="unstyled catList">
                                <li> <a> Drinks</a> </li>
                                <li> <a>Soft</a> </li>
                                <li> <a>Coffee</a> </li>
                                <li> <a>Juice</a> </li>
                                <li> <a>Hot Chocolate & Nutritional Drinks</a> </li>
                                <li> <a >Baking</a> </li>
                            </ul>
                        </div>
                        <div class="col-lg-9 col-md-9 col-sm-9 categoryContainer">
                            <div class="breadcrumbDiv">
                                <ul class="breadcrumb">
                                    <li><a href="<?php echo baseUrl(); ?>index.php">Home</a> <span class="divider">/</span></li>
                                    <li class="active">Beverages</li>
                                </ul>
                            </div>

                            <div class="FeaturedImage"> <img src="<?php echo baseUrl(); ?>images/category/banner.jpg"> </div>

                            <div class="section-title text-center">
                                <h2><a href="category-level-2.html"><span>Soft Drinks  </span> </a> <a class="pull-right see-all" href="category-level-2.html"> See all <i class="fa fa-angle-double-right"></i></a> </h2>
                            </div>
                            <div class="productFeatured categoryProduct xs3Response">

                                <div id="categorySlider" class="tnm-carousel tnm-theme w100 carousel">

                                    <div class="product">
                                        <div class="productBox"> <a href=""><img src="https://s3-ap-southeast-1.amazonaws.com/media.redmart.com/newmedia/150x/i/m/img_5197.jpg"></a>
                                            <div class="description">
                                                <h4><a href="san-remo-spaghetti">San Remo Spaghetti No. 5</a></h4>
                                                <span class="size">500 G</span> </div>
                                            <div class="price"> <span>$2.50</span> </div>
                                            <div class="cartControll"> <a class="btncart"> <span class="counter"> <span>1</span> </span> <span class="add2cart">Add to cart </span> </a> </div>
                                        </div>
                                    </div>
                                    <div class=" product">
                                        <div class="productBox"> <img src="https://s3-ap-southeast-1.amazonaws.com/media.redmart.com/newmedia/150x/i/m/img_2398.jpg">
                                            <div class="description">
                                                <h4><a href="san-remo-spaghetti">San Remo Spaghetti No. 5</a></h4>
                                                <span class="size">500 G</span> </div>
                                            <div class="price"> <span>$2.50</span> </div>
                                            <div class="cartControll"> <a class="btncart"> <span class="counter"> <span>0</span> <span>1</span> <span>2</span> <span>3</span> <span>4</span> <span>5</span> <span>6</span> <span>7</span> <span>8</span> <span>9</span> <span>10</span> <span>11</span> <span>12</span> </span> <span class="add2cart">Add to cart </span> </a> </div>
                                        </div>
                                    </div>
                                    <div class="product">
                                        <div class="productBox"> <a href="#"><img src="https://s3-ap-southeast-1.amazonaws.com/media.redmart.com/newmedia/150x/i/m/img_7205.jpg"></a>
                                            <div class="description">
                                                <h4><a href="san-remo-spaghetti">San Remo Spaghetti No. 5</a></h4>
                                                <span class="size">500 G</span> </div>
                                            <div class="price"> <span>$2.50</span> </div>
                                            <div class="cartControll"> <a class="btncart"> <span class="counter"> <span>0</span> <span>1</span> <span>2</span> <span>3</span> <span>4</span> <span>5</span> <span>6</span> <span>7</span> <span>8</span> <span>9</span> <span>10</span> <span>11</span> <span>12</span> </span> <span class="add2cart">Add to cart </span> </a> </div>
                                        </div>
                                    </div>
                                    <div class=" product">
                                        <div class="productBox"> <img src="https://s3-ap-southeast-1.amazonaws.com/media.redmart.com/newmedia/150x/i/m/img_6470.jpg">
                                            <div class="description">
                                                <h4><a href="san-remo-spaghetti">San Remo Spaghetti No. 5</a></h4>
                                                <span class="size">500 G</span> </div>
                                            <div class="price"> <span>$2.50</span></div>
                                            <div class="cartControll"> <a class="btncart"> <span class="counter"> <span>0</span> <span>1</span> <span>2</span> <span>3</span> <span>4</span> <span>5</span> <span>6</span> <span>7</span> <span>8</span> <span>9</span> <span>10</span> <span>11</span> <span>12</span> </span> <span class="add2cart">Add to cart </span> </a> </div>
                                        </div>
                                    </div>

                                    <div class="product">
                                        <div class="productBox"> <a href=""><img src="https://s3-ap-southeast-1.amazonaws.com/media.redmart.com/newmedia/150x/i/m/img_5197.jpg"></a>
                                            <div class="description">
                                                <h4><a href="san-remo-spaghetti">San Remo Spaghetti No. 5</a></h4>
                                                <span class="size">500 G</span> </div>
                                            <div class="price"> <span>$2.50</span> </div>
                                            <div class="cartControll"> <a class="btncart"> <span class="counter"> <span>1</span> </span> <span class="add2cart">Add to cart </span> </a> </div>
                                        </div>
                                    </div>
                                    <div class=" product">
                                        <div class="productBox"> <img src="https://s3-ap-southeast-1.amazonaws.com/media.redmart.com/newmedia/150x/i/m/img_2398.jpg">
                                            <div class="description">
                                                <h4><a href="san-remo-spaghetti">San Remo Spaghetti No. 5</a></h4>
                                                <span class="size">500 G</span> </div>
                                            <div class="price"> <span>$2.50</span> </div>
                                            <div class="cartControll"> <a class="btncart"> <span class="counter"> <span>0</span> <span>1</span> <span>2</span> <span>3</span> <span>4</span> <span>5</span> <span>6</span> <span>7</span> <span>8</span> <span>9</span> <span>10</span> <span>11</span> <span>12</span> </span> <span class="add2cart">Add to cart </span> </a> </div>
                                        </div>
                                    </div>
                                    <div class="product">
                                        <div class="productBox"> <a href="#"><img src="https://s3-ap-southeast-1.amazonaws.com/media.redmart.com/newmedia/150x/i/m/img_7205.jpg"></a>
                                            <div class="description">
                                                <h4><a href="san-remo-spaghetti">San Remo Spaghetti No. 5</a></h4>
                                                <span class="size">500 G</span> </div>
                                            <div class="price"> <span>$2.50</span> </div>
                                            <div class="cartControll"> <a class="btncart"> <span class="counter"> <span>0</span> <span>1</span> <span>2</span> <span>3</span> <span>4</span> <span>5</span> <span>6</span> <span>7</span> <span>8</span> <span>9</span> <span>10</span> <span>11</span> <span>12</span> </span> <span class="add2cart">Add to cart </span> </a> </div>
                                        </div>
                                    </div>
                                    <div class=" product">
                                        <div class="productBox"> <img src="https://s3-ap-southeast-1.amazonaws.com/media.redmart.com/newmedia/150x/i/m/img_6470.jpg">
                                            <div class="description">
                                                <h4><a href="san-remo-spaghetti">San Remo Spaghetti No. 5</a></h4>
                                                <span class="size">500 G</span> </div>
                                            <div class="price"> <span>$2.50</span></div>
                                            <div class="cartControll"> <a class="btncart"> <span class="counter"> <span>0</span> <span>1</span> <span>2</span> <span>3</span> <span>4</span> <span>5</span> <span>6</span> <span>7</span> <span>8</span> <span>9</span> <span>10</span> <span>11</span> <span>12</span> </span> <span class="add2cart">Add to cart </span> </a> </div>
                                        </div>
                                    </div>


                                    <div class="product">
                                        <div class="productBox"> <a href=""><img src="https://s3-ap-southeast-1.amazonaws.com/media.redmart.com/newmedia/150x/i/m/img_5197.jpg"></a>
                                            <div class="description">
                                                <h4><a href="san-remo-spaghetti">San Remo Spaghetti No. 5</a></h4>
                                                <span class="size">500 G</span> </div>
                                            <div class="price"> <span>$2.50</span> </div>
                                            <div class="cartControll"> <a class="btncart"> <span class="counter"> <span>1</span> </span> <span class="add2cart">Add to cart </span> </a> </div>
                                        </div>
                                    </div>
                                    <div class=" product">
                                        <div class="productBox"> <img src="https://s3-ap-southeast-1.amazonaws.com/media.redmart.com/newmedia/150x/i/m/img_2398.jpg">
                                            <div class="description">
                                                <h4><a href="san-remo-spaghetti">San Remo Spaghetti No. 5</a></h4>
                                                <span class="size">500 G</span> </div>
                                            <div class="price"> <span>$2.50</span> </div>
                                            <div class="cartControll"> <a class="btncart"> <span class="counter"> <span>0</span> <span>1</span> <span>2</span> <span>3</span> <span>4</span> <span>5</span> <span>6</span> <span>7</span> <span>8</span> <span>9</span> <span>10</span> <span>11</span> <span>12</span> </span> <span class="add2cart">Add to cart </span> </a> </div>
                                        </div>
                                    </div>
                                    <div class="product">
                                        <div class="productBox"> <a href="#"><img src="https://s3-ap-southeast-1.amazonaws.com/media.redmart.com/newmedia/150x/i/m/img_7205.jpg"></a>
                                            <div class="description">
                                                <h4><a href="san-remo-spaghetti">San Remo Spaghetti No. 5</a></h4>
                                                <span class="size">500 G</span> </div>
                                            <div class="price"> <span>$2.50</span> </div>
                                            <div class="cartControll"> <a class="btncart"> <span class="counter"> <span>0</span> <span>1</span> <span>2</span> <span>3</span> <span>4</span> <span>5</span> <span>6</span> <span>7</span> <span>8</span> <span>9</span> <span>10</span> <span>11</span> <span>12</span> </span> <span class="add2cart">Add to cart </span> </a> </div>
                                        </div>
                                    </div>
                                    <div class=" product">
                                        <div class="productBox"> <img src="https://s3-ap-southeast-1.amazonaws.com/media.redmart.com/newmedia/150x/i/m/img_6470.jpg">
                                            <div class="description">
                                                <h4><a href="san-remo-spaghetti">San Remo Spaghetti No. 5</a></h4>
                                                <span class="size">500 G</span> </div>
                                            <div class="price"> <span>$2.50</span></div>
                                            <div class="cartControll"> <a class="btncart"> <span class="counter"> <span>0</span> <span>1</span> <span>2</span> <span>3</span> <span>4</span> <span>5</span> <span>6</span> <span>7</span> <span>8</span> <span>9</span> <span>10</span> <span>11</span> <span>12</span> </span> <span class="add2cart">Add to cart </span> </a> </div>
                                        </div>
                                    </div>



                                </div> <!-- slider -->

                            </div>


                            <div class="section-title text-center">
                                <h2><a href=""><span>Breakfast & Cereal  </span> </a><a class="pull-right see-all" href="#"> See all <i class="fa fa-angle-double-right"></i> </a> </h2>
                            </div>
                            <div class="productFeatured categoryProduct">
                                <div id="categorySlider2" class="tnm-carousel tnm-theme w100 carousel">
                                    <div class="product">
                                        <div class="productBox"> <img src="https://s3-ap-southeast-1.amazonaws.com/media.redmart.com/newmedia/150x/i/m/IMG_7279.JPG">
                                            <div class="description">
                                                <h4><a href="san-remo-spaghetti">San Remo Spaghetti No. 5</a></h4>
                                                <span class="size">500 G</span> </div>
                                            <div class="price"> <span>$2.50</span></div>
                                            <div class="cartControll"> <a class="btncart"> <span class="counter"> <span>0</span> <span>1</span> <span>2</span> <span>3</span> <span>4</span> <span>5</span> <span>6</span> <span>7</span> <span>8</span> <span>9</span> <span>10</span> <span>11</span> <span>12</span> </span> <span class="add2cart">Add to cart </span> </a> </div>
                                        </div>
                                    </div>
                                    <div class="product">
                                        <div class="productBox"> <img src="https://s3-ap-southeast-1.amazonaws.com/media.redmart.com/newmedia/150x/i/m/IMG_6333.JPG">
                                            <div class="description">
                                                <h4><a href="san-remo-spaghetti">San Remo Spaghetti No. 5</a></h4>
                                                <span class="size">500 G</span> </div>
                                            <div class="price"> <span>$2.50</span> </div>
                                            <div class="cartControll"> <a class="btncart"> <span class="counter"> <span>0</span> <span>1</span> <span>2</span> <span>3</span> <span>4</span> <span>5</span> <span>6</span> <span>7</span> <span>8</span> <span>9</span> <span>10</span> <span>11</span> <span>12</span> </span> <span class="add2cart">Add to cart </span> </a> </div>
                                        </div>
                                    </div>
                                    <div class=" product">
                                        <div class="productBox"> <a href="" ><img src="https://s3-ap-southeast-1.amazonaws.com/media.redmart.com/newmedia/150x/i/m/IMG_3012.JPG" ></a>
                                            <div class="description">
                                                <h4><a href="san-remo-spaghetti" >San Remo Spaghetti No. 5</a></h4>
                                                <span class="size">500 G</span> </div>
                                            <div class="price"> <span >$2.50</span> </div>
                                            <div class="cartControll"> <a class="btncart" > <span class="counter"> <span>1</span> </span> <span class="add2cart">Add to cart </span> </a> </div>
                                        </div>
                                    </div>
                                    <div class="product">
                                        <div class="productBox"> <img src="https://s3-ap-southeast-1.amazonaws.com/media.redmart.com/newmedia/150x/i/m/img_0462.jpg" >
                                            <div class="description">
                                                <h4><a href="san-remo-spaghetti" >San Remo Spaghetti No. 5</a></h4>
                                                <span class="size">500 G</span> </div>
                                            <div class="price"> <span >$2.50</span> </div>
                                            <div class="cartControll"> <a class="btncart" > <span class="counter"> <span>0</span> <span>1</span> <span>2</span> <span>3</span> <span>4</span> <span>5</span> <span>6</span> <span>7</span> <span>8</span> <span>9</span> <span>10</span> <span>11</span> <span>12</span> </span> <span class="add2cart">Add to cart </span> </a> </div>
                                        </div>
                                    </div>
                                    <div class="product">
                                        <div class="productBox"> <img src="https://s3-ap-southeast-1.amazonaws.com/media.redmart.com/newmedia/150x/i/m/IMG_7279.JPG">
                                            <div class="description">
                                                <h4><a href="san-remo-spaghetti">San Remo Spaghetti No. 5</a></h4>
                                                <span class="size">500 G</span> </div>
                                            <div class="price"> <span>$2.50</span></div>
                                            <div class="cartControll"> <a class="btncart"> <span class="counter"> <span>0</span> <span>1</span> <span>2</span> <span>3</span> <span>4</span> <span>5</span> <span>6</span> <span>7</span> <span>8</span> <span>9</span> <span>10</span> <span>11</span> <span>12</span> </span> <span class="add2cart">Add to cart </span> </a> </div>
                                        </div>
                                    </div>
                                    <div class="product">
                                        <div class="productBox"> <img src="https://s3-ap-southeast-1.amazonaws.com/media.redmart.com/newmedia/150x/i/m/IMG_6333.JPG">
                                            <div class="description">
                                                <h4><a href="san-remo-spaghetti">San Remo Spaghetti No. 5</a></h4>
                                                <span class="size">500 G</span> </div>
                                            <div class="price"> <span>$2.50</span> </div>
                                            <div class="cartControll"> <a class="btncart"> <span class="counter"> <span>0</span> <span>1</span> <span>2</span> <span>3</span> <span>4</span> <span>5</span> <span>6</span> <span>7</span> <span>8</span> <span>9</span> <span>10</span> <span>11</span> <span>12</span> </span> <span class="add2cart">Add to cart </span> </a> </div>
                                        </div>
                                    </div>
                                    <div class=" product">
                                        <div class="productBox"> <a href="" ><img src="https://s3-ap-southeast-1.amazonaws.com/media.redmart.com/newmedia/150x/i/m/IMG_3012.JPG" ></a>
                                            <div class="description">
                                                <h4><a href="san-remo-spaghetti" >San Remo Spaghetti No. 5</a></h4>
                                                <span class="size">500 G</span> </div>
                                            <div class="price"> <span >$2.50</span> </div>
                                            <div class="cartControll"> <a class="btncart" > <span class="counter"> <span>1</span> </span> <span class="add2cart">Add to cart </span> </a> </div>
                                        </div>
                                    </div>
                                    <div class="product">
                                        <div class="productBox"> <img src="https://s3-ap-southeast-1.amazonaws.com/media.redmart.com/newmedia/150x/i/m/img_0462.jpg" >
                                            <div class="description">
                                                <h4><a href="san-remo-spaghetti" >San Remo Spaghetti No. 5</a></h4>
                                                <span class="size">500 G</span> </div>
                                            <div class="price"> <span >$2.50</span> </div>
                                            <div class="cartControll"> <a class="btncart" > <span class="counter"> <span>0</span> <span>1</span> <span>2</span> <span>3</span> <span>4</span> <span>5</span> <span>6</span> <span>7</span> <span>8</span> <span>9</span> <span>10</span> <span>11</span> <span>12</span> </span> <span class="add2cart">Add to cart </span> </a> </div>
                                        </div>
                                    </div>

                                    <div class="product">
                                        <div class="productBox"> <img src="https://s3-ap-southeast-1.amazonaws.com/media.redmart.com/newmedia/150x/i/m/IMG_7279.JPG">
                                            <div class="description">
                                                <h4><a href="san-remo-spaghetti">San Remo Spaghetti No. 5</a></h4>
                                                <span class="size">500 G</span> </div>
                                            <div class="price"> <span>$2.50</span></div>
                                            <div class="cartControll"> <a class="btncart"> <span class="counter"> <span>0</span> <span>1</span> <span>2</span> <span>3</span> <span>4</span> <span>5</span> <span>6</span> <span>7</span> <span>8</span> <span>9</span> <span>10</span> <span>11</span> <span>12</span> </span> <span class="add2cart">Add to cart </span> </a> </div>
                                        </div>
                                    </div>
                                    <div class="product">
                                        <div class="productBox"> <img src="https://s3-ap-southeast-1.amazonaws.com/media.redmart.com/newmedia/150x/i/m/IMG_6333.JPG">
                                            <div class="description">
                                                <h4><a href="san-remo-spaghetti">San Remo Spaghetti No. 5</a></h4>
                                                <span class="size">500 G</span> </div>
                                            <div class="price"> <span>$2.50</span> </div>
                                            <div class="cartControll"> <a class="btncart"> <span class="counter"> <span>0</span> <span>1</span> <span>2</span> <span>3</span> <span>4</span> <span>5</span> <span>6</span> <span>7</span> <span>8</span> <span>9</span> <span>10</span> <span>11</span> <span>12</span> </span> <span class="add2cart">Add to cart </span> </a> </div>
                                        </div>
                                    </div>
                                    <div class=" product">
                                        <div class="productBox"> <a href="" ><img src="https://s3-ap-southeast-1.amazonaws.com/media.redmart.com/newmedia/150x/i/m/IMG_3012.JPG" ></a>
                                            <div class="description">
                                                <h4><a href="san-remo-spaghetti" >San Remo Spaghetti No. 5</a></h4>
                                                <span class="size">500 G</span> </div>
                                            <div class="price"> <span >$2.50</span> </div>
                                            <div class="cartControll"> <a class="btncart" > <span class="counter"> <span>1</span> </span> <span class="add2cart">Add to cart </span> </a> </div>
                                        </div>
                                    </div>
                                    <div class="product">
                                        <div class="productBox"> <img src="https://s3-ap-southeast-1.amazonaws.com/media.redmart.com/newmedia/150x/i/m/img_0462.jpg" >
                                            <div class="description">
                                                <h4><a href="san-remo-spaghetti" >San Remo Spaghetti No. 5</a></h4>
                                                <span class="size">500 G</span> </div>
                                            <div class="price"> <span >$2.50</span> </div>
                                            <div class="cartControll"> <a class="btncart" > <span class="counter"> <span>0</span> <span>1</span> <span>2</span> <span>3</span> <span>4</span> <span>5</span> <span>6</span> <span>7</span> <span>8</span> <span>9</span> <span>10</span> <span>11</span> <span>12</span> </span> <span class="add2cart">Add to cart </span> </a> </div>
                                        </div>
                                    </div>



                                </div> <!-- slider end-->
                            </div>



                            <div class="section-title text-center">
                                <h2><a href=""><span>Breakfast & Cereal  </span> </a><a class="pull-right see-all" href="#"> See all <i class="fa fa-angle-double-right"></i> </a> </h2>
                            </div>
                            <div class="productFeatured categoryProduct">
                                <div id="categorySlider3" class="tnm-carousel tnm-theme w100 carousel">
                                    <div class="product">
                                        <div class="productBox"> <img src="https://s3-ap-southeast-1.amazonaws.com/media.redmart.com/newmedia/150x/i/m/IMG_7279.JPG">
                                            <div class="description">
                                                <h4><a href="san-remo-spaghetti">San Remo Spaghetti No. 5</a></h4>
                                                <span class="size">500 G</span> </div>
                                            <div class="price"> <span>$2.50</span></div>
                                            <div class="cartControll"> <a class="btncart"> <span class="counter"> <span>0</span> <span>1</span> <span>2</span> <span>3</span> <span>4</span> <span>5</span> <span>6</span> <span>7</span> <span>8</span> <span>9</span> <span>10</span> <span>11</span> <span>12</span> </span> <span class="add2cart">Add to cart </span> </a> </div>
                                        </div>
                                    </div>
                                    <div class="product">
                                        <div class="productBox"> <img src="https://s3-ap-southeast-1.amazonaws.com/media.redmart.com/newmedia/150x/i/m/IMG_6333.JPG">
                                            <div class="description">
                                                <h4><a href="san-remo-spaghetti">San Remo Spaghetti No. 5</a></h4>
                                                <span class="size">500 G</span> </div>
                                            <div class="price"> <span>$2.50</span> </div>
                                            <div class="cartControll"> <a class="btncart"> <span class="counter"> <span>0</span> <span>1</span> <span>2</span> <span>3</span> <span>4</span> <span>5</span> <span>6</span> <span>7</span> <span>8</span> <span>9</span> <span>10</span> <span>11</span> <span>12</span> </span> <span class="add2cart">Add to cart </span> </a> </div>
                                        </div>
                                    </div>
                                    <div class=" product">
                                        <div class="productBox"> <a href="" ><img src="https://s3-ap-southeast-1.amazonaws.com/media.redmart.com/newmedia/150x/i/m/IMG_3012.JPG" ></a>
                                            <div class="description">
                                                <h4><a href="san-remo-spaghetti" >San Remo Spaghetti No. 5</a></h4>
                                                <span class="size">500 G</span> </div>
                                            <div class="price"> <span >$2.50</span> </div>
                                            <div class="cartControll"> <a class="btncart" > <span class="counter"> <span>1</span> </span> <span class="add2cart">Add to cart </span> </a> </div>
                                        </div>
                                    </div>
                                    <div class="product">
                                        <div class="productBox"> <img src="https://s3-ap-southeast-1.amazonaws.com/media.redmart.com/newmedia/150x/i/m/img_0462.jpg" >
                                            <div class="description">
                                                <h4><a href="san-remo-spaghetti" >San Remo Spaghetti No. 5</a></h4>
                                                <span class="size">500 G</span> </div>
                                            <div class="price"> <span >$2.50</span> </div>
                                            <div class="cartControll"> <a class="btncart" > <span class="counter"> <span>0</span> <span>1</span> <span>2</span> <span>3</span> <span>4</span> <span>5</span> <span>6</span> <span>7</span> <span>8</span> <span>9</span> <span>10</span> <span>11</span> <span>12</span> </span> <span class="add2cart">Add to cart </span> </a> </div>
                                        </div>
                                    </div>
                                    <div class="product">
                                        <div class="productBox"> <img src="https://s3-ap-southeast-1.amazonaws.com/media.redmart.com/newmedia/150x/i/m/IMG_7279.JPG">
                                            <div class="description">
                                                <h4><a href="san-remo-spaghetti">San Remo Spaghetti No. 5</a></h4>
                                                <span class="size">500 G</span> </div>
                                            <div class="price"> <span>$2.50</span></div>
                                            <div class="cartControll"> <a class="btncart"> <span class="counter"> <span>0</span> <span>1</span> <span>2</span> <span>3</span> <span>4</span> <span>5</span> <span>6</span> <span>7</span> <span>8</span> <span>9</span> <span>10</span> <span>11</span> <span>12</span> </span> <span class="add2cart">Add to cart </span> </a> </div>
                                        </div>
                                    </div>
                                    <div class="product">
                                        <div class="productBox"> <img src="https://s3-ap-southeast-1.amazonaws.com/media.redmart.com/newmedia/150x/i/m/IMG_6333.JPG">
                                            <div class="description">
                                                <h4><a href="san-remo-spaghetti">San Remo Spaghetti No. 5</a></h4>
                                                <span class="size">500 G</span> </div>
                                            <div class="price"> <span>$2.50</span> </div>
                                            <div class="cartControll"> <a class="btncart"> <span class="counter"> <span>0</span> <span>1</span> <span>2</span> <span>3</span> <span>4</span> <span>5</span> <span>6</span> <span>7</span> <span>8</span> <span>9</span> <span>10</span> <span>11</span> <span>12</span> </span> <span class="add2cart">Add to cart </span> </a> </div>
                                        </div>
                                    </div>
                                    <div class=" product">
                                        <div class="productBox"> <a href="" ><img src="https://s3-ap-southeast-1.amazonaws.com/media.redmart.com/newmedia/150x/i/m/IMG_3012.JPG" ></a>
                                            <div class="description">
                                                <h4><a href="san-remo-spaghetti" >San Remo Spaghetti No. 5</a></h4>
                                                <span class="size">500 G</span> </div>
                                            <div class="price"> <span >$2.50</span> </div>
                                            <div class="cartControll"> <a class="btncart" > <span class="counter"> <span>1</span> </span> <span class="add2cart">Add to cart </span> </a> </div>
                                        </div>
                                    </div>
                                    <div class="product">
                                        <div class="productBox"> <img src="https://s3-ap-southeast-1.amazonaws.com/media.redmart.com/newmedia/150x/i/m/img_0462.jpg" >
                                            <div class="description">
                                                <h4><a href="san-remo-spaghetti" >San Remo Spaghetti No. 5</a></h4>
                                                <span class="size">500 G</span> </div>
                                            <div class="price"> <span >$2.50</span> </div>
                                            <div class="cartControll"> <a class="btncart" > <span class="counter"> <span>0</span> <span>1</span> <span>2</span> <span>3</span> <span>4</span> <span>5</span> <span>6</span> <span>7</span> <span>8</span> <span>9</span> <span>10</span> <span>11</span> <span>12</span> </span> <span class="add2cart">Add to cart </span> </a> </div>
                                        </div>
                                    </div>

                                    <div class="product">
                                        <div class="productBox"> <img src="https://s3-ap-southeast-1.amazonaws.com/media.redmart.com/newmedia/150x/i/m/IMG_7279.JPG">
                                            <div class="description">
                                                <h4><a href="san-remo-spaghetti">San Remo Spaghetti No. 5</a></h4>
                                                <span class="size">500 G</span> </div>
                                            <div class="price"> <span>$2.50</span></div>
                                            <div class="cartControll"> <a class="btncart"> <span class="counter"> <span>0</span> <span>1</span> <span>2</span> <span>3</span> <span>4</span> <span>5</span> <span>6</span> <span>7</span> <span>8</span> <span>9</span> <span>10</span> <span>11</span> <span>12</span> </span> <span class="add2cart">Add to cart </span> </a> </div>
                                        </div>
                                    </div>
                                    <div class="product">
                                        <div class="productBox"> <img src="https://s3-ap-southeast-1.amazonaws.com/media.redmart.com/newmedia/150x/i/m/IMG_6333.JPG">
                                            <div class="description">
                                                <h4><a href="san-remo-spaghetti">San Remo Spaghetti No. 5</a></h4>
                                                <span class="size">500 G</span> </div>
                                            <div class="price"> <span>$2.50</span> </div>
                                            <div class="cartControll"> <a class="btncart"> <span class="counter"> <span>0</span> <span>1</span> <span>2</span> <span>3</span> <span>4</span> <span>5</span> <span>6</span> <span>7</span> <span>8</span> <span>9</span> <span>10</span> <span>11</span> <span>12</span> </span> <span class="add2cart">Add to cart </span> </a> </div>
                                        </div>
                                    </div>
                                    <div class=" product">
                                        <div class="productBox"> <a href="" ><img src="https://s3-ap-southeast-1.amazonaws.com/media.redmart.com/newmedia/150x/i/m/IMG_3012.JPG" ></a>
                                            <div class="description">
                                                <h4><a href="san-remo-spaghetti" >San Remo Spaghetti No. 5</a></h4>
                                                <span class="size">500 G</span> </div>
                                            <div class="price"> <span >$2.50</span> </div>
                                            <div class="cartControll"> <a class="btncart" > <span class="counter"> <span>1</span> </span> <span class="add2cart">Add to cart </span> </a> </div>
                                        </div>
                                    </div>
                                    <div class="product">
                                        <div class="productBox"> <img src="https://s3-ap-southeast-1.amazonaws.com/media.redmart.com/newmedia/150x/i/m/img_0462.jpg" >
                                            <div class="description">
                                                <h4><a href="san-remo-spaghetti" >San Remo Spaghetti No. 5</a></h4>
                                                <span class="size">500 G</span> </div>
                                            <div class="price"> <span >$2.50</span> </div>
                                            <div class="cartControll"> <a class="btncart" > <span class="counter"> <span>0</span> <span>1</span> <span>2</span> <span>3</span> <span>4</span> <span>5</span> <span>6</span> <span>7</span> <span>8</span> <span>9</span> <span>10</span> <span>11</span> <span>12</span> </span> <span class="add2cart">Add to cart </span> </a> </div>
                                        </div>
                                    </div>



                                </div> <!-- slider end-->
                            </div>

                        </div>
                    </div>
                </div>



            </div>

            <?php include basePath('footer.php'); ?>



        </div>
        <!-- /wrapper --> 




        <?php include basePath('mini_login.php'); ?>
        <?php include basePath('mini_signup.php'); ?>
        <?php include basePath('mini_cart.php'); ?>

        <?php include basePath('footer_script.php'); ?>
    </body>
</html>