<?php
include 'config/config.php';
if(!checkUserLogin()){
  $err = "You need to signup/signin first.";
  $link = baseUrl() . "user-signin-signup?err=" . base64_encode($err) . "&checkout=true";
  redirect($link);
} else {
  $UserID = getSession('UserID'); 
}

$promotionCode = '';
$promotionDiscount = '';

if(isset($_SESSION['Coupon-No']) AND $_SESSION['Coupon-No'] != ""){
  $promotionCode = $_SESSION['Coupon-No'];
}

if(isset($_SESSION['Coupon-Discount']) AND $_SESSION['Coupon-Discount'] > 0){
  $promotionDiscount = $_SESSION['Coupon-Discount'];
}

$payment = 0;
$CartID = session_id();


$page_title = get_option('SITE_DEFAULT_META_TITLE');
$page_description = get_option('SITE_DEFAULT_META_DESCRIPTION');
$page_keywords = get_option('SITE_DEFAULT_META_KEYWORDS');
$site_author = $config['CONFIG_SETTINGS']['SITE_AUTHOR'];

if(!isset($_GET['shipping']) OR !isset($_GET['billing'])){
  $link = baseUrl() . 'checkout-step-1';
  redirect($link);
}


if(isset($_POST['submit'])){
  extract($_POST);
  if($payment == 0){
    $err = "Please select a payment method.";
  } else {
    $link = baseUrl() . "checkout-step-3/" . $_GET['shipping'] . "/" . $_GET['billing'] . "/" . $payment;
    redirect($link);
  }
}



$sqlWholeTempCart = "SELECT SUM(TC_product_total_price) AS TotalPrice, SUM(TC_discount_amount) AS TotalDiscount FROM temp_carts WHERE TC_session_id='$CartID'";
$executeWholeCart = mysqli_query($con,$sqlWholeTempCart);
if($executeWholeCart){
  $executeWholeCartObj = mysqli_fetch_object($executeWholeCart);
  $CartTotalPrice = $executeWholeCartObj->TotalPrice;
  $CartTotalDiscount = $executeWholeCartObj->TotalDiscount;
  $grandTotal = number_format(($CartTotalPrice - $CartTotalDiscount), 2);
} else {
  $data["error"] = 3;
  if(DEBUG){
    $data["error_text"]= 'executeWholeCart error: ' . mysqli_error($con);
  } else {
    $data["error_text"]= 'executeWholeCart query failed';
  }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title><?php echo $page_title; ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="<?php echo $page_description; ?>">
    <meta name="keywords" content="<?php echo $page_keywords; ?>">
    <meta name="author" content="<?php echo $site_author; ?>">

        <?php include basePath('header_script.php'); ?>
        <script src="<?php echo baseUrl(); ?>ajax/index/main.js"></script>

<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

</head>

<body>
<div id="wrapper">
  
  <div id="header">
      <div class="navbar navbar-default navbar-fixed-top megamenu">
          <div class="container-full">
              <?php include basePath('headertop.php'); ?>
              <!--/.headertop -->
              <?php include basePath('header_mid.php'); ?>
              <!--/.headerBar -->

              <?php include basePath('header_menu.php'); ?>
              <!--/.menubar --> 
          </div>
      </div>

  </div>
  <!-- header end -->
  
  <div style="clear:both"></div>
  <div class="w100 mainContainer">
    <div class="container" style="padding-top:50px">
      <div class="row ">
        <div class="col-lg-12 checkoutBar">
          <div class="col-lg-4 col-md-4 col-xs-4 col-sm-4 "> <a class="checkPoint"> <span> <img src="<?php echo baseUrl(); ?>images/site/step1.png"> </span> </a> <a class="checkPointName arrow_top" href="#"> Select Address </a> </div>
<!--          <div class="col-lg-3 col-md-3 col-xs-3 col-sm-3 "> <a class="checkPoint"><span> <img src="images/site/step2.png"> </span></a> <a class="checkPointName arrow_top" > Select Delivery </a> </div>-->
          <div class="col-lg-4 col-md-4 col-xs-4 col-sm-4 "> <a class="checkPoint"><span> <img src="<?php echo baseUrl(); ?>images/site/step3.png"> </span></a> <a class="checkPointName arrow_top active" >Select Payment</a> </div>
          <div class="col-lg-4 col-md-4 col-xs-4 col-sm-4 "> <a class="checkPoint"><span> <img src="<?php echo baseUrl(); ?>images/site/step4.png"> </span></a> <a class="checkPointName arrow_top" >Review and Confirm</a> </div>
        </div>
      </div>
    </div>
    <div class="container">
      
      <div class="row ">
        <?php include basePath('alert.php'); ?>
      </div>
      
      <form action="<?php echo baseUrl(); ?>checkout-step-2/<?php echo $_GET['shipping']; ?>/<?php echo $_GET['billing']; ?>" method="post">
      <div class="row">
        <div class="col-lg-8 col-md-8 col-sm-8 col-xm-12">
          <div class="cartContent">
            <div class="paymentHeading noborder">
              <h2>Payment Method</h2>
            </div>
            <div class="row">
              <div class="col-lg-6 col-md-6">
                <div class="box highlight">
                  <div class="box-content highlight">
                    <div class="radio">
                      <label>
                        <input type="radio" name="payment" value="1">
                        <img alt="bkash" src="<?php echo baseUrl(); ?>images/bkash.png"> Bkash
                      </label>
                    </div>
                  </div>
                  <hgroup class="title">

                    <h5>The easiest and safest way to send or receive money instantly on your mobile 
                      <strong>01680531352</strong> </h5>
                  </hgroup>

                </div>
              </div>
              <div class="col-lg-6 col-md-6">
                <div class="box highlight">
                  <div class="box-content highlight">
                    <div class="radio">
                      <label>
                        <input type="radio" name="payment" value="2">
                        <img style="width: 21px;" src="<?php echo baseUrl(); ?>images/cash_on_delivery.png" alt="cash on delivery"> Cash on Delivery</label>
                    </div>
                  </div>
                  <hgroup class="title">

                    <h5>Cash on Delivery is one of the payment methods for making purchases on bajaree.com. 
                    </h5>
                  </hgroup>

                </div>
              </div>
            </div>
          </div>
          <!--product content--> 
          
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xm-12">
          
          <div class="cartRight">
            <table style="border-bottom:1px solid #ddd;" width="100%" border="0">
              <tr>
                <td align="left">Subtotal</td>
                <td align="left"><?php echo $config['CURRENCY_SIGN']; ?> <?php echo number_format($CartTotalPrice,2); ?></td>
              </tr>
              <tr id="cartTotalDiscount">
              <?php if($promotionDiscount > 0): ?>
              
                <td align="left">Discount</td>
                <td align="left"><?php echo $config['CURRENCY_SIGN']; ?> <?php echo number_format($promotionDiscount,2); ?></td>
              
              <?php endif; ?>
              </tr>
              <tr>
                <td>Delivery</td>
                <td><strong class="free">FREE!</strong></td>
              </tr>
            </table>
            
            <div class="cartRightInner">
              <input value="<?php echo $promotionCode; ?>" placeholder="Coupon code..." name="code" id="couponNo">
              <button type="button" name="cbtn" class="btn btn-default btn-site" onclick="discountCoupon();">enter</button>
              <h5>Terms &amp; Conditions</h5>
              <p>
                1. Limited to use of 1 coupon per checkout. <br />
                2. After 3 wrong submission, coupon system will lock down automatically. 
              </p>
            </div>
            
            <div class="totalrow">
              <div class="text">
                <h3>Total</h3>
              </div>
              <div class="total">
                <?php if($promotionDiscount > 0): ?>
                <h3 id="grandTotal"><?php echo $config['CURRENCY_SIGN']; ?> <?php echo number_format($grandTotal - $promotionDiscount,2); ?></h3>
                <?php else: ?>
                <h3 id="grandTotal"><?php echo $config['CURRENCY_SIGN']; ?> <?php echo $grandTotal; ?></h3>
                <?php endif; ?>
              </div>
            </div>
          </div>
          <h3><button type="submit" class="btn btn-site btn-lg btn-block" name="submit">Next <i class="fa fa-long-arrow-right"></i></button></h3>
        </div>
      </div>
      </form>
    </div>
    
    <!--brandFeatured--> 
    
  </div>
  <!-- Main hero unit -->
  
  <?php include basePath('footer.php'); ?>
</div>
<!-- /container --> 

        <?php include basePath('mini_login.php'); ?>
        <?php include basePath('mini_signup.php'); ?>
        <?php include basePath('mini_cart.php'); ?>

        <?php include basePath('footer_script.php'); ?>
</body>
</html>
