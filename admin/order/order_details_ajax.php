<?php
include ('../../config/config.php');
$ProductID = $_GET['ProductID'];
$InventoryID = $_GET['InventoryID'];
$OrderID = $_GET['OrderID'];

$sqlOrderProduct = "SELECT 
                products.product_title,
                product_inventories.PI_inventory_title,
                (SELECT product_images.PI_file_name FROM product_images WHERE product_images.PI_inventory_id = order_products.OP_product_inventory_id AND product_images.PI_product_id = products.product_id ORDER BY product_images.PI_priority DESC LIMIT 1) as PI_file_name,
                order_products.OP_product_quantity,
                product_sizes.PS_size_title

                FROM order_products

                LEFT JOIN product_inventories ON product_inventories.PI_id = order_products.OP_product_inventory_id
                LEFT JOIN product_discounts ON product_discounts.PD_inventory_id = order_products.OP_product_inventory_id
                LEFT JOIN products ON products.product_id = order_products.OP_product_id
                LEFT JOIN product_sizes ON product_sizes.PS_size_id = product_inventories.PI_size_id
                WHERE order_products.OP_order_id=$OrderID
                AND order_products.OP_product_id=$ProductID
                AND order_products.OP_product_inventory_id=$InventoryID";
$executeOrderProduct = mysqli_query($con, $sqlOrderProduct);
if ($executeOrderProduct) {
  $executeOrderProductObj = mysqli_fetch_object($executeOrderProduct);
  if(isset($executeOrderProductObj->product_title)){
    echo '<img src="'.baseUrl('upload/product/mid/' . $executeOrderProductObj->PI_file_name).'" width="200px" align="middle" style="margin-left:50px;" />
          <table cellpadding="0" cellspacing="0" width="100%" class="tableStatic">
      <thead>
          <tr>
            <td width="25%">Title</td>
            <td width="25%">Value</td>
            <td width="25%">Title</td>
            <td width="25%">Value</td>
          </tr>
      </thead>
      <tbody>
          <tr>
              <td><strong>Title</strong></td>
              <td><strong>'.$executeOrderProductObj->product_title.'</strong></td>
              <td><strong>Inventory Title</strong></td>  
              <td><strong>'.$executeOrderProductObj->PI_inventory_title.'</strong></td>
          </tr>
          <tr>
              <td>Size:</td>
              <td><strong>'.$executeOrderProductObj->PS_size_title.'</strong></td>
              <td>Quantity:</td>
              <td><strong>'.$executeOrderProductObj->OP_product_quantity.'</strong></td>
          </tr>
       </tbody>
    </table>';
  }
} else {
  if (DEBUG) {
    echo "executeTempCart error: " . mysqli_error($con);
  }
}
			
?>