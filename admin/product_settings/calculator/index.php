<?php
include ('../../../config/config.php');
if (!checkAdminLogin()) {
    $link = baseUrl('admin/index.php?err=' . base64_encode('Please login to access admin panel'));
    redirect($link);
}
$CategoryID = 0;
$TotalPrice = 0;
$TotalProduct = 0;

//get all level one category
$arrayCategory = array();
$sqlCategory = "SELECT * FROM categories WHERE category_parent_id=2";
$executeCategory = mysqli_query($con,$sqlCategory);
if($executeCategory){
  while($executeCategoryObj = mysqli_fetch_object($executeCategory)){
    $arrayCategory[] = $executeCategoryObj;
  }
} else {
  if(DEBUG){
    echo "executeCategory error: " . mysqli_error($con);
  } else {
    echo "executeCategory query failed.";
  }
}



if(isset($_GET['submit']) AND isset($_GET['category'])){
  $CategoryID = base64_decode($_GET['category']);
  $allCategories = '';
  $getAllCategory = array();
  $sqlGetCategories = "SELECT category_id 
    
                      FROM categories 
                      
                      WHERE category_parent_id IN 
                      (SELECT category_id FROM categories WHERE category_id=$CategoryID 
                        UNION 
                       SELECT category_id FROM categories WHERE category_parent_id=$CategoryID)";
  $executeGetCategories = mysqli_query($con,$sqlGetCategories);
  if($executeGetCategories){
    while($executeGetCategoriesObj = mysqli_fetch_object($executeGetCategories)){
      $getAllCategory[] = $executeGetCategoriesObj->category_id;
    }
  } else {
    if(DEBUG){
      echo "executeGetCategories error: " . mysqli_error($con);
    } else {
      echo "executeGetCategories query failed.";
    }
  }
  
  
  $allCategories = implode(',', $getAllCategory);
  
  $sqlGetCategoryInfo = "SELECT DISTINCT PI_id, COUNT(PC_product_id) AS TotalProduct, SUM(PI_current_price) AS TotalPrice
                          
                         FROM product_categories
                         
                         LEFT JOIN product_inventories ON PI_product_id=PC_product_id
                         WHERE PC_category_id IN ($allCategories)";
  $executeGetCategoryInfo = mysqli_query($con,$sqlGetCategoryInfo);
  if($executeGetCategoryInfo){
    $executeGetCategoryInfoObj = mysqli_fetch_object($executeGetCategoryInfo);
    if(isset($executeGetCategoryInfoObj->TotalProduct)){
      $TotalPrice = $executeGetCategoryInfoObj->TotalPrice;
      $TotalProduct = $executeGetCategoryInfoObj->TotalProduct;
    }
  }
}


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Admin Panel | Category-wise Calculator</title>

        <?php
        include basePath('admin/header.php');
        ?>



        <!--Start: tree view--> 

        <link href="<?php echo baseUrl('admin/css/tree_style.css'); ?>" rel="stylesheet" type="text/css" />

       
    </head>

    <body>


        <?php include basePath('admin/top_navigation.php'); ?>

        <?php include basePath('admin/module_link.php'); ?>


        <!-- Content wrapper -->
        <div class="wrapper">

            <!-- Left navigation -->

            <?php include basePath('admin/product_settings/product_settings_left_navigation.php'); ?>
            <!-- Content Start -->
            <div class="content">
                <div class="title"><h5>Calculator Module</h5></div>

                <!-- Notification messages -->
                <?php include basePath('admin/message.php'); ?>


                <!-- Charts -->
                <div class="widget first">
                    <div class="head">
                        <h5 class="iGraph">Category-wise Calculator</h5></div>
                    <div class="body">
                        <div class="charts" style="width: 700px; height: auto;">
                            <form action="index.php" method="get" class="mainForm" enctype="multipart/form-data">

                                <!-- Input text fields -->
                                <fieldset>
                                    <div class="widget first">
                                        <div class="head"><h5 class="iList">Category-wise Calculator</h5></div>
                                        
                                        <div class="rowElem noborder"><label>Select Category:</label>
                                          <div class="formRight">
                                            <select name="category">
                                              <option value="">-- Category --</option>
                                              <?php
                                              $countCategoryArray = count($arrayCategory);
                                              if($countCategoryArray > 0):
                                                for($i = 0; $i < $countCategoryArray; $i++):
                                                  
                                                  $catID = $arrayCategory[$i]->category_id;
                                                  $catTitle = $arrayCategory[$i]->category_name;
                                                  ?>
                                              <option value="<?php echo base64_encode($catID); ?>" <?php if($CategoryID == $catID){ echo "selected"; } ?>><?php echo $catTitle; ?></option>
                                                  <?php
                                                endfor;
                                              endif;
                                              ?>
                                            </select>
                                          </div>
                                          <div class="fix"></div>
                                        </div>
                                        
                                      <?php if(isset($_GET['submit']) AND isset($_GET['category'])){ ?>
                                        <div class="stats">
                                          <ul>
                                            <label>Category Information:</label>
                                            <li><a href="#" class="count grey" title=""><?php echo $TotalProduct; ?></a><br><span>Total Product</span></li>
                                            <li><a href="#" class="count grey" title=""><?php if($TotalPrice == ''){ echo '0.00'; }else{ echo $TotalPrice; } ?></a><br><span>Total Price</span></li>
                                            
                                          </ul>
                                          <div class="fix"></div>
                                        </div>

                                      <?php } ?>
                                        
                                        <div class="fix"></div>
                                        <div class="fix"></div>

                                        <input type="submit" name="submit" value="View Info" class="greyishBtn submitForm" />
                                        <div class="fix"></div>

                                        
                                        <div class="fix"></div>

                                    </div>   
                                </fieldset>

                            </form>		

                        </div>






                    </div>





                </div>
            </div>

        </div>
        <!-- Content End -->

        <div class="fix"></div>
        </div>

        <?php include basePath('admin/footer.php'); ?>
