<?php
include 'config/config.php';

$page_title = get_option('SITE_DEFAULT_META_TITLE');
$page_description = get_option('SITE_DEFAULT_META_DESCRIPTION');
$page_keywords = get_option('SITE_DEFAULT_META_KEYWORDS');
$site_author = $config['CONFIG_SETTINGS']['SITE_AUTHOR'];


?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title><?php echo $page_title; ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="<?php echo $page_description; ?>">
    <meta name="keywords" content="<?php echo $page_keywords; ?>">
    <meta name="author" content="<?php echo $site_author; ?>">

    <?php include basePath('header_script.php'); ?>
    <script src="<?php echo baseUrl(); ?>ajax/index/main.js"></script>
<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

</head>

<body>
<div id="wrapper">
  
  
  <div id="header">
        <div class="navbar navbar-default navbar-fixed-top megamenu">
          <div class="container-full">
            <?php include basePath('headertop.php'); ?>
            <!--/.headertop -->
            <?php include basePath('header_mid.php'); ?>
            <!--/.headerBar -->

            <?php include basePath('header_menu.php'); ?>
            <!--/.menubar --> 
          </div>
        </div>

      </div>
      <!-- header end -->
  
  <div id="header">
    <div class="navbar navbar-default navbar-fixed-top megamenu">
      <div class="container-full">
        <div class="headertop">
          <div class="container">
            <nav id="contentNav"> <a class="zendeskHelp" href="">HELP</a> <a target="_blank" href="">FAQ</a> <a target="_blank" href="">APPS</a> <a class="callUs" href="callto:+88026458">0265 8546</a> </nav>
          </div>
        </div>
        <!--/.headertop -->
        
        <div class="headerBar navbar-inverse">
          <div class="container"> <a class="brand navbar-brand" href="index.html"> <img height="48" src="images/bajaree.com-Final-Logo-5.png"> </a>
            <ul class="nav navbar-nav">
              <!-- Classic list -->
              <li class="searhLi">
                <div class="input-append searchBar">
                  <input type="text" class="span2 searchField" id="appendedInputButton">
                  <button class="btn searchBtn" type="button"> <i class="fa fa-search"> </i></button>
                </div>
              </li>
              <div style="clear:both;" class="hide show-ipod"></div>
              <li class="hr-nav hover"> <a class="myList"> My List </a> </li>
              <li class="hr-nav hover"> <a class="myCart" id="myCart" data-toggle="modal" data-target="#ModalCart"> Cart</a> </li>
              <li class="logBtn logBtnFirst"> <a class=" btn-bj btn-log" data-toggle="modal" data-target="#ModalLogin"> Login </a> </li>
              <li class="logBtn"> <a class=" btn-bj btn-signup" data-toggle="modal" data-target="#ModalSignup"> Signup </a> </li>
            </ul>
          </div>
        </div>
        <!--/.headerBar -->
        
      </div>
    </div>
    <div class="headerMenu">
      <div class="navbar navbar-default megamenu">
        <div class="navbar-inner">
          <button data-target=".nav-collapse" data-toggle="collapse" class="btn btn-navbar" type="button"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
          <div class="headerBar navbar-inverse">
            <div class=" container">
              <div class="nav-collapse collapse">
                <div class="nav-collapse collapse nav1 userNav" id="nav1">
                  <ul class="nav ">
                    <!-- Classic list -->
                    <li class="searhLi">
                      <div class="input-append searchBar">
                        <input class="col-lg-2 searchField" id="appendedInputButton" type="text">
                        <button class="btn searchBtn" type="button"> <i class="fa fa-search"> </i></button>
                      </div>
                    </li>
                    <li class="hr-nav hover"> <a><img src="images/mylist.png"> My List </a> </li>
                    <li class="cartA hover"> <a> <img src="images/cart.png"> Cart</a> </li>
                    <li class=""> <a href="#ModalLogin" data-toggle="modal" class=" btn-bj btn-log"> Login </a> </li>
                    <li class=""> <a href="#ModalSignup" data-toggle="modal" class=" btn-bj btn-signup"> Signup </a> </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          
          <!--/.nav-collapse --> 
        </div>
        <!-- /.navbar-inner --> 
      </div>
      <!-- /.navbar --> 
      <!-- menu end --> 
    </div>
  </div>
  <!-- header end -->
  
  <div class="row" style="margin:0;">
  	<div class="about_banner">
    <h1 align="center"><img src="images/bajaree.com-Final-Logo-5.png" class="contact_logo" width="150" alt="logo"></h1>
    <p>Our mission is to save you time and money<br>
for the important things in life!</p></div>
  </div>
  <div class="w100 mainContainer contactPage ">

       <div class="container">
       <div class="col-lg-3 col-md-2 col-sm-3 col-xs-12 main-column">
       <div class="about_cntImg"> 
				<p align="center"><img width="150" src="images/map.png" alt="map"></p>
 		 </div>
    
    	</div>
        
       <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 main-column">
       <div class="about_content"> 
					<h4>Contact us</h4>
                    <p>TOTAL LINK Nobarun Bhaban H # 39/4<br>
Road # 22. Banani,Dhaka, 
Kamal Ataturk Avenue,Banani,Dhaka-1213<br>
 Banani Bazar, Dhaka - 1213</p>
 		 </div>
    
    	</div>
        
  
       </div>
    
    
    
    <!--brandFeatured-->
    
  </div>
  <!-- Main hero unit -->
  
  <?php include basePath('footer.php'); ?>
  
</div>
<!-- /container --> 

    <?php include basePath('mini_login.php'); ?>
    <?php include basePath('mini_signup.php'); ?>
    <?php include basePath('mini_cart.php'); ?>

    <?php include basePath('footer_script.php'); ?>
    
    
  </body>
</html>
