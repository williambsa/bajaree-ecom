<!--<footer>
  <div class="footer">
    <div class="container">
      <div class="row">
        <div class="col-lg-3  col-md-3 col-sm-4 col-xs-6">
          <h3>About</h3>
          <ul>
            <li><a href="<?php echo baseUrl(); ?>contact.php">Contact</a></li>
            <li><a href="<?php echo baseUrl(); ?>faq.php">FAQ</a></li>
            <li><a href="<?php echo baseUrl(); ?>condition.php">Terms and condition</a></li>
          </ul>
        </div>   

        <div class="col-lg-3  col-md-3 col-sm-6 col-xs-12">
          <h3>Customer Service </h3>
          <?php echo $config['CONFIG_SETTINGS']['FOOTER_CUSTOMER_SERVICE']; ?>
        </div>


        <div class="col-lg-3  col-md-3 col-sm-4 col-xs-12">
          <h3>My Bajaree</h3>
          <ul>
            <li><a href="<?php echo baseUrl(); ?>account">My Account</a></li>
            <li><a href="<?php echo baseUrl(); ?>my-orders">Order History</a></li>
            <li><a href="<?php echo baseUrl(); ?>my-wishlist">Wish List</a></li>
          </ul>
        </div>


        <div class="col-lg-3  col-md-3 col-sm-6 col-xs-12">
          <h3>Stay in touch</h3>

          <ul class="social">
            <li><a href="http://www.facebook.com/Bajareesuperstore"><i class=" fa fa-facebook">&nbsp;</i></a></li>
            <li><a href="https://twitter.com/BajareeInfo"><i class="fa fa-twitter">&nbsp;</i></a></li>
            <li><a href="https://plus.google.com"><i class="fa fa-google-plus">&nbsp;</i></a></li>
            <li><a href="http://youtube.com"><i class="fa fa-youtube">&nbsp;</i></a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <div class="footer-bottom">
    <div class="container">
      <p class="pull-left"> � Bajaree 2014. All right reserved.</p>
      <img src="images/site/paypal.png" class="pull-right"> </div>
  </div>
</footer>
-->






<footer>
  <div class="footer">
  <div class="footer-banner">
   <div class="container text-center"><h1><img src="<?php echo baseUrl(); ?>images/bajaree-footerB.png" class="img-responsive" alt="banner"></h1>
   </div>
  </div>
  <div class="footerNavbar">
          <div class="container">
              <div class="col-lg-6 col-md-6 col-sm-8 col-xs-12">
            <ul class="list-inline pull-left">
        <li><a href="#"> Order Online</a></li>
        <li><a href="#">About</a></li>
        <li><a href="#">Team</a></li>
         <li><a href="#">Press</a></li>
          <li><a href="#">Careers</a></li>
           <li><a href="#">Contact Us</a></li>
      </ul>
      </div>
      <div class="col-lg-6 col-md-6 col-sm-4 col-xs-12">
           <p class="btmPhone"><i class="fa fa-phone"></i> 24/7 Call: 18 (033) 234 5678 </p>
      </div>
      </div>

        </div>
    <div class="container">
             <div class="col-lg-2 col-md-2 col-sm-4 col-xs-4">
                
 <h3>My Bajaree</h3>
          <ul>
             <li><a href="<?php echo baseUrl(); ?>account">My Account</a></li>
            <li><a class="fotrMyCart" href="<?php echo baseUrl(); ?>/my-cart">My Cart</a></li>
            <li><a href="<?php echo baseUrl(); ?>my-orders">Order History</a></li>
            <li><a href="<?php echo baseUrl(); ?>my-wishlist">Wish List</a></li>
          </ul>
          
           </div>
        
        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4">
         <h3>Customer Service</h3>
          <ul>
           <li><a href="<?php echo baseUrl(); ?>faq.php">FAQ</a></li>
            <li><a href="javascript:void(0)"> Vendor Map</a></li>
            <li><a href="<?php echo baseUrl(); ?>contact.php">Contact Us</a></li>
            <li><a href="<?php echo baseUrl(); ?>condition.php"> Privacy Policy</a></li>
          </ul>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4">
          <h3>For Business</h3>
          <ul>
           <li><a href="<?php echo baseUrl(); ?>faq.php">FAQ</a></li>
            <li><a href="<?php echo baseUrl(); ?>my-wishlist"> How it Works</a></li>
            <li><a href="<?php echo baseUrl(); ?>contact.php">Customer Support</a></li>
            <li><a href="<?php echo baseUrl(); ?>condition.php">Corporate Accounts</a></li>
          </ul>
        </div>
        
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 newsLatterSubscribe">
          <h3>GET IN TOUCH</h3>
          <p style="padding-top: 10px;">Made with Love in Dhaka. WANT MORE DETAILS,
GET CONNECT WITH US.</p>
          <ul class="list-inline">
            
            <?php if(get_option('FACEBOOK_LINK') != ''): ?>
            <li><a href="<?php echo get_option('FACEBOOK_LINK'); ?>" target="_blank"><i class="fa fa-facebook"></i></a></li>
            <?php else: ?>
            <li><a href="javascript:void(0)"><i class="fa fa-facebook"></i></a></li>
            <?php endif; ?>
            
            <?php if(get_option('TWITTER_LINK') != ''): ?>
            <li><a href="<?php echo get_option('TWITTER_LINK'); ?>" target="_blank"><i class="fa fa-twitter"></i></a></li>
            <?php else: ?>
            <li><a href="javascript:void(0)"><i class="fa fa-twitter"></i></a></li>
            <?php endif; ?>
            
            <?php if(get_option('GOOGLE_PLUS_LINK') != ''): ?>
            <li><a href="<?php echo get_option('GOOGLE_PLUS_LINK'); ?>" target="_blank"><i class="fa fa-google-plus"></i></a></li>
            <?php else: ?>
            <li><a href="javascript:void(0)"><i class="fa fa-google-plus"></i></a></li>
            <?php endif; ?>
            
            <?php if(get_option('LINKEDIN_LINK') != ''): ?>
            <li><a href="<?php echo get_option('LINKEDIN_LINK'); ?>" target="_blank"><i class="fa fa-linkedin"></i></a></li>
            <?php else: ?>
            <li><a href="javascript:void(0)"><i class="fa fa-linkedin"></i></a></li>
            <?php endif; ?>
            
            <?php if(get_option('PINTEREST_LINK') != ''): ?>
            <li><a href="<?php echo get_option('PINTEREST_LINK'); ?>" target="_blank"><i class="fa fa-pinterest"></i></a></li>
            <?php else: ?>
            <li><a href="javascript:void(0)"><i class="fa fa-pinterest"></i></a></li>
            <?php endif; ?>
          </ul>
        </div>
    </div>
  </div>
  <div class="footer-bottom">
    <div class="container text-center">
      <p>Copyright � 2014 Bluescheme Inc. All rights reserved.</p> </div>
  </div>
</footer>