<?php 
//require ('config.php');

//if(isset($_POST['submit'])){
//
//    $uname = mysql_escape_string($_POST['uname']);
//    $pass = mysql_escape_string($_POST['pass']);
//
//    $pass = md5($pass);
//
//    $sql = mysql_query("SELECT * FROM `users` WHERE `uname` = '$uname' AND `pass` = '$pass'");
//    if(mysql_num_rows($sql) > 0){
//        header('Location: dashboard.html');
//    }else{
//        echo '<script>
//        $(function(){$(".login-alert").hide().html(data).fadeIn();};);
//        </script>';
//    }
//}
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>MIHRM SYSTEM</title>
<link rel="stylesheet" href="css/style.default.css" type="text/css" />
<link rel="stylesheet" href="css/style.shinyblue.css" type="text/css" />

<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script type="text/javascript" src="js/jquery-migrate-1.1.1.min.js"></script>
<!--<script type="text/javascript" src="js/jquery-ui-1.9.2.min.js"></script>-->
<script type="text/javascript" src="js/modernizr.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/jquery.cookie.js"></script>
<script type="text/javascript" src="js/custom.js"></script>
<script type="text/javascript">
    function checkValue(){
      var u = $('#uname').val();
      var p = $('#pass').val();
      if(u == '' || p == '') {
        alert('empty');
      } else {
        alert('full');
        $( "form#login" ).submit();
      }
    }
</script>
</head>

    <body class="loginpage">

    <div class="loginpanel">
        <div class="loginpanelinner">
            <div class="logo animate0 bounceIn"><img src="images/rnllogo.png" alt="" /></div>
            <form id="login" action="login.php" method="post">
                <div class="inputwrapper login-alert">
                    <div class="alert alert-error">Invalid username or password</div>
                </div>
                <div class="inputwrapper animate1 bounceIn">
                    <input type="text" name="uname" id="uname" placeholder="Enter any username" />
                </div>
                <div class="inputwrapper animate2 bounceIn">
                    <input type="password" name="pass" id="pass" placeholder="Enter any password" />
                </div>
                <div class="inputwrapper animate3 bounceIn">
                  <button type="button" name="submit" onClick="checkValue();">Sign In</button>
                </div>
                <div class="inputwrapper animate4 bounceIn">
                    <label><input type="checkbox" class="remember" name="signin" /> Keep me sign in</label>
                </div>

            </form>
        </div><!--loginpanelinner-->
    </div><!--loginpanel-->

    <div class="loginfooter">
        <p>&copy; 2013. MIHRM. All Rights Reserved.</p>
    </div>

    </body>
</html>