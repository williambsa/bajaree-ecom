<?php
include 'config/config.php';
include ('./lib/email/mail_helper_functions.php');

$CartID = '';
$loginEmail = '';
$loginPass = '';
$email = '';
$pass = '';
$fname = '';
$checkout = '';

$page_title = get_option('SITE_DEFAULT_META_TITLE');
$page_description = get_option('SITE_DEFAULT_META_DESCRIPTION');
$page_keywords = get_option('SITE_DEFAULT_META_KEYWORDS');
$site_author = $config['CONFIG_SETTINGS']['SITE_AUTHOR'];

if (isset($_GET['checkout'])) {
    $checkout = $_GET['checkout'];
}

//login user
if (isset($_POST['login'])) {
    extract($_POST);
    if ($loginEmail == '') {
        $err = "Email Address/User Name is required.";
    } elseif ($loginPass == '') {
        $err = "Password is required.";
    } else {
        $emailCount = 0;
        $sqlCheck = "SELECT * FROM users WHERE user_email='".  mysqli_real_escape_string($con, $loginEmail)."'";
        $executeCheck = mysqli_query($con, $sqlCheck);
        if ($executeCheck) {
            $emailCount = mysqli_num_rows($executeCheck);
            if ($emailCount > 0) {

                //getting user information from database
                $executeCheckResult = mysqli_fetch_object($executeCheck);
                //printDie($executeCheckResult, TRUE);
                $securePass = securedPass($loginPass);
                if ($executeCheckResult->user_status != 'active') {
                    $err = "Your account is inactive. Please contact with site administrator."; // User not active
                } elseif ($executeCheckResult->user_verification != 'yes') {
                    $err = "Your email address is not verified , An email was sent when you create account."; // Email not verified
                } elseif ($executeCheckResult->user_password != $securePass) {
                    $loginPass = "";
                    $err = "Password is not correct. Please retrive your password from Forgotten Password link."; // Email not verified
                }else{
                    //checking if user already added product to cart
                    $countTempCartProduct = 0;
                    $sqlTempCart = "SELECT * FROM temp_carts WHERE TC_session_id='$CartID'";
                    $executeTempCart = mysqli_query($con, $sqlTempCart);
                    if ($executeTempCart) {
                        $countTempCartProduct = mysqli_num_rows($executeTempCart);
                        if ($countTempCartProduct > 0) {
                            //updating userid into temp cart
                            $updateCart = '';
                            $updateCart .= ' TC_user_id = "' . intval($executeCheckResult->user_id) . '"';

                            $sqlUpdateCart = "UPDATE temp_carts SET $updateCart WHERE TC_session_id='$CartID'";
                            $executeUpdateCart = mysqli_query($con, $sqlUpdateCart);
                            if (!$executeUpdateCart) {
                                if (DEBUG) {
                                    $err = "executeUpdateCart error: " . mysqli_error($con); // executeUpdateCart query failed
                                } else {
                                    $err = "executeUpdateCart query failed"; // executeUpdateCart query failed
                                }
                            }
                        }
                    } else {
                        if (DEBUG) {
                            echo "executeTempCart error: " . mysqli_error($con);
                        } else {
                            echo "executeTempCart query failed";
                        }
                    }

                    setSession('UserID', $executeCheckResult->user_id);
                    setSession('Email', $executeCheckResult->user_email);
                    setSession('UserFirstName', $executeCheckResult->user_first_name);
                    setSession('UserName', $executeCheckResult->user_name);

                    // User signed in successfully
                    if (isset($_GET['checkout']) AND $_GET['checkout'] == 'true') {
                        $link = baseUrl() . 'checkout-step-1?msg=' . base64_encode('You logged in successfully.');
                        redirect($link);
                    } else {
                        $link = baseUrl() . 'my-account?msg=' . base64_encode('You logged in successfully.');
                        redirect($link);
                    }
                }
            } else {
                $err = "This Email Address/User Name is not registered."; // Username is not registered
            }
        } else {
            if (DEBUG) {
                $err = "executeCheck error: " . mysqli_error($con); // Username is not registered
            } else {
                $err = "Email Address/User Name check failed. Try again."; // Username is not registered
            }
        }
    }
}



//signing up user
if (isset($_POST['register'])) {

    extract($_POST);

    if ($email == '') {
        $err = "Email Address is required.";
    } elseif ($pass == '') {
        $err = "Password is required.";
    } elseif ($fname == '') {
        $err = "Name is required.";
    } else {
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $err = "Please provide a valid Email Address."; // Incorrect email address format
        } else {
            $emailCount = 0;
            $sqlCheck = "SELECT * FROM users WHERE user_email='".  mysqli_real_escape_string($con, $email)."'";
            $executeCheck = mysqli_query($con, $sqlCheck);
            if ($executeCheck) {
                $emailCount = mysqli_num_rows($executeCheck);
                if ($emailCount > 0) {
                    $err = "This Email already exist."; // Email address already registered with database
                } else {

                    $securePass = securedPass($pass);
                    $userHash = session_id();

                    $AddUser = '';
                    $AddUser .= ' user_email = "' . mysqli_real_escape_string($con, $email) . '"';
                    $AddUser .= ', user_password = "' . mysqli_real_escape_string($con, $securePass) . '"';
                    $AddUser .= ', user_first_name = "' . mysqli_real_escape_string($con, $fname) . '"';
                    $AddUser .= ', user_hash = "' . mysqli_real_escape_string($con, $userHash) . '"';

                    $sqlAddUser = "INSERT INTO users SET $AddUser";
                    $executeAddUser = mysqli_query($con, $sqlAddUser);
                    if ($executeAddUser) {
                        //setting up user session values
                        $userID = mysqli_insert_id($con);
                        setSession('FirstName', $fname);
                        setSession('UserID', $userID);
                        setSession('Email', $email);


                        //sending email to user
                        $Subject = "Registration confirmation from bajaree.com";
                        $EmailBody = file_get_contents(baseUrl('emails/signup/signup.body.php?user_id=' . $userID));
                        $sendEmailToApplicant = sendEmailFunction($email, $fname, 'no-reply@bajaree.com', $Subject, $EmailBody);


                        if ($sendEmailToApplicant) {

                            //checking if user already added product to cart
                            $countTempCartProduct = 0;
                            $sqlTempCart = "SELECT * FROM temp_carts WHERE TC_session_id='$CartID'";
                            $executeTempCart = mysqli_query($con, $sqlTempCart);
                            if ($executeTempCart) {
                                $countTempCartProduct = mysqli_num_rows($executeTempCart);
                                if ($countTempCartProduct > 0) {
                                    //updating userid into temp cart
                                    $updateCart = '';
                                    $updateCart .= ' TC_user_id = "' . intval($userID) . '"';

                                    $sqlUpdateCart = "UPDATE temp_carts SET $updateCart WHERE TC_session_id='$CartID'";
                                    $executeUpdateCart = mysqli_query($con, $sqlUpdateCart);
                                    if (!$executeUpdateCart) {
                                        if (DEBUG) {
                                            $err = "executeUpdateCart error: " . mysqli_error($con); // Incorrect email address format
                                        } else {
                                            $err = "executeUpdateCart query failed"; // Incorrect email address format
                                        }
                                    }
                                }
                            } else {
                                if (DEBUG) {
                                    $err = "executeTempCart error: " . mysqli_error($con); // Incorrect email address format
                                } else {
                                    $err = "executeTempCart query failed"; // Incorrect email address format
                                }
                            }
                        } else {
                            $err = "Email send failed"; // Incorrect email address format
                        }

                        // User signed in successfully
                        if (isset($_GET['checkout']) AND $_GET['checkout'] == 'true') {
                            $link = baseUrl() . 'checkout-step-1?msg=' . base64_encode('Signup process was successfull. Please check your inbox.');
                            redirect($link);
                        } else {
                            $link = baseUrl() . 'my-account?msg=' . base64_encode('Signup process was successfull. Please check your inbox.');
                            redirect($link);
                        }
                    } else {
                        if (DEBUG) {
                            $err = "executeAddUser error: " . mysqli_error($con); // $executeAddUser failed
                        } else {
                            $err = "executeAddUser query failed."; // $executeAddUser failed
                        }
                    }
                }
            } else {
                if (DEBUG) {
                    $err = "executeCheck error: " . mysqli_error($con); // $executeCheck query failed
                } else {
                    $err = "executeCheck query failed."; // $executeCheck query failed
                }
            }
        }
    }
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title><?php echo $page_title; ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="<?php echo $page_description; ?>">
        <meta name="keywords" content="<?php echo $page_keywords; ?>">
        <meta name="author" content="<?php echo $site_author; ?>">

        <?php include basePath('header_script.php'); ?>

    </head>

    <body>
        <div id="wrapper">


            <div id="wrapper">
                <div id="header">
                    <div class="navbar navbar-default navbar-fixed-top megamenu">
                        <div class="container-full">
                            <?php include basePath('headertop.php'); ?>
                            <!--/.headertop -->
                            <?php include basePath('header_mid.php'); ?>
                            <!--/.headerBar -->

                            <?php include basePath('header_menu.php'); ?>
                            <!--/.menubar --> 
                        </div>
                    </div>

                </div>
                <!-- header end -->

                <div class="w100 mainContainer">


                    <div class="container">

                        <div class="row">
                            <?php include basePath('alert.php'); ?>
                        </div>

                        <div id="content"> 
                            <h1 class="noborder">Account Login</h1>
                            <div class="login-content">
                                <div class="row">
                                    <div class="col-lg-5 col-md-4 col-sm-6 col-xs-12">
                                        <div class="inner userRegistration">
                                            <h2>New Customer</h2>
                                            <div class="content">
                                                <p>New in here? Its easy to open account.</p>
                                                <form enctype="multipart/form-data" method="post" action="<?php echo baseUrl(); ?>user-signin-signup?<?php if ($checkout != '') {
                                echo 'checkout=' . $checkout;
                            } ?>" autocomplete="off">
                                                    <div class="content">
                                                        <b>Name:</b><br>
                                                        <input type="text" required value="<?php echo $fname; ?>" name="fname" class="form-control" required>
                                                        <br>
                                                        <b>E-Mail Address:</b><br>
                                                        <input type="email" required value="<?php echo $email; ?>" name="email" class="form-control" required>
                                                        <br>
                                                        <b>Password:</b><br>
                                                        <input type="password" required value="<?php echo $pass; ?>" name="pass" class="form-control" required>

                                                        <br>
                                                        <input type="hidden" value="?route=account/account" name="redirect">
                                                    </div>
                                                    <button type="submit" name="register" class="btn btn-site" href="user_account_registration.html"><i class="fa fa-plus"></i> Register </button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-5  col-md-5 col-sm-6 col-xs-12 col-md-offset-1">
                                        <div class="inner userRegistration">
                                            <h2>Returning Customer</h2>
                                            <form enctype="multipart/form-data" method="post" action="<?php echo baseUrl(); ?>user-signin-signup?<?php if ($checkout != '') {
                                echo 'checkout=' . $checkout;
                            } ?>" autocomplete="off">
                                                <div class="content">
                                                    <p>Already have an account? Login below.</p>
                                                    <b>E-Mail Address:</b><br>
                                                    <input type="eamil" required value="<?php echo $loginEmail; ?>" name="loginEmail" class="form-control" required>
                                                    <br>
                                                    <b>Password:</b><br>
                                                    <input type="password" required value="<?php echo $loginPass; ?>" name="loginPass" class="form-control" required>
                                                    <br>
                                                    <a href="/forgot-my-password">Forgotten Password</a><br>
                                                    <br>
                                                    <button type="submit" name="login" class="btn btn-site"><i class="fa fa-sign-in"></i> Login</button>
                                                    <input type="hidden" value="?route=account/account" name="redirect">
                                                </div>
                                            </form>
                                        </div>
                                    </div>	


                                </div>	
                            </div>
                        </div>

                    </div>



                    <!--brandFeatured-->

                </div>
                <!-- Main hero unit -->

            <?php include basePath('footer.php'); ?>

            </div>
            <?php include basePath('mini_login.php'); ?>
            <?php include basePath('mini_signup.php'); ?>
<?php include basePath('mini_cart.php'); ?>

<?php include basePath('footer_script.php'); ?>


    </body>
</html>
