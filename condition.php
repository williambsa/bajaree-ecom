<?php
include 'config/config.php';

$page_title = get_option('SITE_DEFAULT_META_TITLE');
$page_description = get_option('SITE_DEFAULT_META_DESCRIPTION');
$page_keywords = get_option('SITE_DEFAULT_META_KEYWORDS');
$site_author = $config['CONFIG_SETTINGS']['SITE_AUTHOR'];


?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title><?php echo $page_title; ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="<?php echo $page_description; ?>">
    <meta name="keywords" content="<?php echo $page_keywords; ?>">
    <meta name="author" content="<?php echo $site_author; ?>">

    <?php include basePath('header_script.php'); ?>
    <script src="<?php echo baseUrl(); ?>ajax/index/main.js"></script>
<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

</head>

<body>
<div id="wrapper">

  
  <div id="header">
        <div class="navbar navbar-default navbar-fixed-top megamenu">
          <div class="container-full">
            <?php include basePath('headertop.php'); ?>
            <!--/.headertop -->
            <?php include basePath('header_mid.php'); ?>
            <!--/.headerBar -->

            <?php include basePath('header_menu.php'); ?>
            <!--/.menubar --> 
          </div>
        </div>

      </div>
      <!-- header end -->
  
  <div class="w100 mainContainer">
  
  
  
  
       <div class="container">
       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 main-column">
       <div class="termsContent"> 
  <h1>Terms & Conditions</h1>
  		
        <p>Bajaree.com ("we," "us," "our") has established these Terms of Use ("Terms") for each customer ("you," "your"), which govern your access to, and use of the www.bajaree.com website (the "Services"), and any information, text, graphics, or other materials uploaded, downloaded, or appearing on the Services (collectively referred to as "Content"). By accessing or using the Services you agree to be bound by these Terms.</p>
        
        <h4>Use of the Services</h4>
        
<p>The Company grants you a single, non-exclusive, limited personal license to access and use the Services conditioned on your continued compliance with these Terms. This license is for the sole purpose of enabling you to use and enjoy the benefit of the Services as provided by the Company, in the manner permitted by these Terms. You may not transfer or share your right to access or use the Services.</p>

<p>You may use the Services if you are of legal age to form a binding contract and are not a person barred from receiving services under the laws of the Republic of Singapore. You may use the Services only in compliance with these Terms and all applicable local, state, national, and international laws, rules and regulations.</p>

<p>All right, title, and interest in and to the Services are and will remain the exclusive property of the Company and its licensors. The Services are protected by copyright, trademark, and other laws, of both the Republic of Singapore and foreign countries. Nothing in the Terms gives you a right to use the Company's name or any of the Company's trademarks, logos, domain names, and other distinctive brand features, and you may not use the same without our express written permission. Any feedback, comments, or suggestions ("Submission") you may provide regarding the Services is entirely voluntary and we will be free to use such Submission as we see fit and without any obligation to you. By making a Submission, you grant us a non-exclusive, royalty-free, perpetual, irrevocable, and fully sub licensable right to use, reproduce, modify, adapt, publish, translate, create derivative works from, distribute, and display such Submission throughout the world in any media. You also grant us the right to use the name you submit in connection with such Submission.</p>

<p>The Services that the Company provides are always evolving and the form and nature of the Services that the Company provides may change from time to time without prior notice to you. In addition, the Company may stop (permanently or temporarily) providing the Services (or any features within the Services) to you or to users generally and may not be able to provide you with prior notice. We also retain the right to create limits on use at our sole discretion at any time without prior notice to you.
</p>
<p>In order to use certain Services, you will need to register at the website, create a username and provide certain information about yourself as prompted by the registration form, including a valid email address. We reserve the right to reclaim usernames that violate these Terms.</p>

<h4>Passwords</h4>

<p>You are responsible for safeguarding the password that you use to access the Services and for any activities or actions under your password. We encourage you to use "strong" passwords (passwords that use a combination of upper and lower case letters, numbers and symbols) with your account. The Company cannot and will not be liable for any loss or damage arising from your failure to comply with the above requirements.</p>

<h4>Privacy</h4>

<p>Any personal information that you provide to the Company is subject to our Privacy Policy, which governs our collection and use of your personal information. You understand that through your use of the Services you consent to the collection and use (as set forth in the Privacy Policy) of this information, including the transfer of this information to other countries for storage, processing, and use by the Company. As part of providing you the Services, we may need to provide you with certain communications, such as service announcements and administrative messages. These communications are considered part of the Services and your account, which you may not be able to opt-out from receiving.</p>

<h4>Content on the Services</h4>

<p>We do not endorse, support, represent, or guarantee the completeness, truthfulness, accuracy, or reliability of any Content or communications posted via the Services or endorse any opinions expressed via the Services. You understand that by using the Services, you may be exposed to Content that might be offensive, harmful, inaccurate, or otherwise inappropriate, or in some cases, postings that have been mislabeled or are otherwise deceptive. Under no circumstances will the Company be liable in any way for any Content, including, but not limited to, any errors or omissions in any Content, or any loss or damage of any kind incurred as a result of the use of any Content posted, emailed, transmitted, or otherwise made available via the Services or broadcast elsewhere.</p>

<h4>Restrictions on the Content and Use of the Services</h4>

<p>We reserve the right at all times (but will not have an obligation) to remove or refuse to distribute any Content on the Services. We also reserve the right to access, read, preserve, and disclose any information as we reasonably believe is necessary to (i) satisfy any applicable law, regulation, legal process or governmental request, (ii) enforce the Terms, including investigation of potential violations hereof, (iii) detect, prevent, or otherwise address fraud, security, or technical issues, (iv) respond to user support requests, or (v) protect the rights, property, or safety of the Company, its users, and the public.</p>

<p>You may not do any of the following while accessing or using the Services: (i) access, tamper with, or use non-public areas of the Services, Company's computer systems, or the technical delivery systems of Company's providers; (ii) probe, scan, or test the vulnerability of any system or network or breach or circumvent any security or authentication measures; (iii) access or search or attempt to access or search the Services by any means (automated or otherwise) other than through our currently available, published interfaces that are provided by the Company (and only pursuant to those terms and conditions), unless you have been specifically allowed to do so in a separate agreement with the Company; (iv) forge any TCP/IP packet header or any part of the header information in any email or posting, or in any way use the Services to send altered, deceptive, or false source-identifying information; or (v) interfere with, or disrupt, (or attempt to do so), the access of any user, host or network, including, without limitation, sending a virus, overloading, flooding, spamming, mail-bombing the Services, or by scripting the creation of Content in such a manner as to interfere with or create an undue burden on the Services.</p>

<h4>Intellectual Property</h4>

<p>You acknowledge that the Services contain information, data, software, photographs, graphics, videos, text, images, typefaces, sounds, and other material (collectively "Content") that are protected by copyrights, trademarks, or other proprietary rights, and that these rights are valid and protected in all forms, media, and technologies existing now or hereinafter developed. You acknowledge that all such Content is proprietary to the Company and/or our suppliers. You may not modify, remove, delete, augment, add to, publish, transmit, participate in the transfer or sale of, create derivative works from, or in any way exploit any of the Content, in whole or in part. If no specific restrictions are displayed, you may make copies of select portions of the Content, provided that the copies are made only for your personal, information, and non-commercial use, and that you do not alter or modify the Content in any way, and maintain any notices contained in the Content, such as all copyright notices, trademark legends, or other proprietary rights notices. Except as provided in the preceding sentence, you may not reproduce, or distribute in any way Content protected by copyright, or other proprietary right, without obtaining permission of the owner of the copyright or other proprietary right.</p>

<h4>Copyright Policy</h4>

<p>The Company respects the intellectual property rights of others. If you believe that your Content has been copied in a way that constitutes copyright infringement, please provide us with the following information: (i) a physical or electronic signature of the copyright owner or a person authorized to act on their behalf; (ii) identification of the copyrighted work claimed to have been infringed; (iii) identification of the material that is claimed to be infringing or to be the subject of infringing activity and that is to be removed or access to which is to be disabled, and information reasonably sufficient to permit us to locate the material; (iv) your contact information, including your address, telephone number, and an email address; (v) a statement by you that you have a good faith belief that use of the material in the manner complained of is not authorized by the copyright owner, its agent, or the law; and (vi) a statement that the information in the notification is accurate, and, under penalty of perjury, that you are authorized to act on behalf of the copyright owner.</p>

<p>We reserve the right to remove Content in violation of this policy without prior notice and at our sole discretion. Our designated copyright agent for notice of alleged copyright infringement appearing on the Services is: Legal Department, bajaree Pte. Ltd. and can be reached at help@bajaree.com.</p>

<h4>Termination</h4>

<p>If you violate any of these Terms, your permission to use the Services may be suspended or revoked, and we reserve the right to suspend or terminate your access to, and use of, the Services at any time.</p>

<h4>The Services are available "AS IS"</h4>

<p>Your access to and use of the Services or any Content is at your own risk. You understand and agree that the Services are provided to you on an "AS IS" and "AS AVAILABLE" basis. Without limiting the foregoing, THE COMPANY AND ITS PARTNERS DISCLAIM ANY WARRANTIES, EXPRESS OR IMPLIED, OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. We make no warranty and disclaim all responsibility and liability for the completeness, accuracy, availability, timeliness, security, or reliability of the Services or any content thereon. The Company will not be responsible or liable for any harm to your computer system, loss of data, or other harm that results from your access to or use of the Services, or any Content. We make no warranty that the Services will meet your requirements or be available on an uninterrupted, secure, or error-free basis. No advice or information, whether oral or written, obtained from the Company or through the Services, will create any warranty not expressly made herein.</p>

<h4>Links</h4>

<p>The Services may contain links to third-party websites or resources. You acknowledge and agree that we are not responsible or liable for: (i) the availability or accuracy of such websites or resources; or (ii) the content, products, or services on or available from such websites or resources. Links to such websites or resources do not imply any endorsement by the Company of such websites or resources, or the content, products, or services available from such websites or resources. You acknowledge sole responsibility for and assume all risk arising from your use of any such websites or resources.</p>

<h4>Indemnity</h4>

<p>If anyone brings a claim against the Company related to your access to or use of the Services, your violation of these Terms, or any Content posted, published, transmitted, or otherwise provided by you or on your behalf, you will indemnify and hold us harmless from and against all damages, losses, and expenses of any kind (including reasonable legal fees and costs) related to such claim.</p>

<h4>Limitation of Liability</h4>

<p>TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW, THE COMPANY AND ITS SUBSIDIARIES, AFFILIATES, OFFICERS, EMPLOYEES, AGENTS, PARTNERS, AND LICENSORS WILL NOT BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, CONSEQUENTIAL, OR PUNITIVE DAMAGES, INCLUDING WITHOUT LIMITATION, LOSS OF PROFITS, DATA, USE, GOODWILL, OR OTHER INTANGIBLE LOSSES, RESULTING FROM (i) YOUR ACCESS TO OR USE OF OR INABILITY TO ACCESS OR USE THE SERVICES; (ii) ANY CONDUCT OR CONTENT OF ANY THIRD PARTY ON THE SERVICES, INCLUDING WITHOUT LIMITATION, ANY DEFAMATORY, OFFENSIVE OR ILLEGAL CONDUCT OF OTHER USERS OR THIRD PARTIES; (iii) ANY CONTENT OBTAINED FROM THE SERVICES; AND (iv) UNAUTHORIZED ACCESS, USE OR ALTERATION OF YOUR TRANSMISSIONS OR CONTENT, WHETHER BASED ON WARRANTY, CONTRACT, TORT (INCLUDING NEGLIGENCE) OR ANY OTHER LEGAL THEORY, WHETHER OR NOT THE COMPANY HAS BEEN INFORMED OF THE POSSIBILITY OF SUCH DAMAGE, AND EVEN IF A REMEDY SET FORTH HEREIN IS FOUND TO HAVE FAILED OF ITS ESSENTIAL PURPOSE.</p>

<h4>Exclusions</h4>

<p>Some jurisdictions do not allow the exclusion of certain warranties or the exclusion or limitation of liability for consequential or incidental damages, so the limitations above may not apply to you.</p>

<h4>Waiver and Severability</h4>

<p>The failure of the Company to enforce any right or provision of these Terms will not be deemed a waiver of such right or provision. In the event that any provision of these Terms is held to be invalid or unenforceable, the remaining provisions of these Terms will remain in full force and effect.</p>

<h4>Controlling Law and Jurisdiction</h4>

<p>These Terms and any action related thereto will be governed by the laws of the Republic of Singapore, without regard to or application of its conflict of law provisions or your state or country of residence. All claims, legal proceedings, or litigation arising in connection with the Services will be brought solely in Singapore, and you consent to the jurisdiction of and venue in such courts and waive any objection as to inconvenient forum.</p>

<h4>Entire Agreement</h4>

<p>These Terms and our Privacy Policy are the entire and exclusive agreement between the Company and you regarding the Services, and these Terms supersede and replace any prior agreements between the Company and you regarding the Services.</p>

<p>We may revise these Terms from time to time, and we will try to provide you notice prior to any new terms taking effect. By continuing to access or use the Services after those revisions become effective, you agree to be bound by the revised Terms.</p>

<p>Effective Feb 10, 2014</p>
  </div>
    
    	</div>
        
  
       </div>
    
    
    
    <!--brandFeatured-->
    
 </div>
  <!-- Main hero unit -->
  
  <?php include basePath('footer.php'); ?>
  
</div>
<!-- /container --> 

    <?php include basePath('mini_login.php'); ?>
    <?php include basePath('mini_signup.php'); ?>
    <?php include basePath('mini_cart.php'); ?>

    <?php include basePath('footer_script.php'); ?>
    
    
  </body>
</html>
